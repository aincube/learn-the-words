import React, { Component } from 'react';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { PrivateRoute } from './utils/privateRoute';

import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import CurrentCard from './pages/CurrentCard';

import { Spin, Layout } from 'antd';

import FirebaseContext from './context/firebaseContext';

import s from './App.module.scss';

const { Content, Header } = Layout;

class App extends Component {
    state = {
        user: null,
    }

    componentDidMount() {
        const { auth, setUserUID } = this.context;
        auth.onAuthStateChanged(user => {
            if (user && !this.state.user) {
                setUserUID(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                this.setState({ user })
            } else {
                setUserUID(null);
                localStorage.removeItem('user');
                this.setState({ user : false })
            }
        });
    };

    render() {
        const { user } = this.state;
        if (user === null){
            return (
                <div className={s.spinner}>
                    <Spin size="large" />
                </div>                
            )
        } else {
            return (
                <BrowserRouter>
                    <Switch>
                        <Route path="/login" component={LoginPage} />

                        <Route render={() => (
                            <>
                                <Switch>
                                    <PrivateRoute path="/" exact component={HomePage} />
                                    <PrivateRoute path="/home/:id?/:isDone?" component={HomePage} />
                                    <PrivateRoute path="/word/:id?" component={CurrentCard} />
                                    <Redirect to="/"/>
                                </Switch>
                            </>
                        )} />
                    </Switch>
                </BrowserRouter>
            );
        }
    }
}

App.contextType = FirebaseContext;

export default App;
