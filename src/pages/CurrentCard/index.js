import React from 'react';
import FirebaseContext from '../../context/firebaseContext';
import { Spin, Typography, Card} from 'antd';

import s from './CurrentCard.module.scss';

const { Title, Text } = Typography;

class CurrentCard extends React.PureComponent {
    state = {
        word: { 
            _id: 0,
            eng: '',
            rus: '',
        }
    }

    componentDidMount() {
        const { getUserCurrentCardRef } = this.context;
        const { match: { params } } = this.props;

        if (params.id) {                        
            getUserCurrentCardRef(params.id)
                .once('value')
                .then(res => { 
                    // TODO: добавить обработку ошибки на несуществующий id
                    this.setState({ word : res.val() })
                })    
        }
    }

    render() {
        const { word: {eng, rus}} = this.state;
        if (eng === '' && rus === '') {
            return <div className={s.spinner}><Spin /></div>
        }

        return (
            <div>
                <Title>Текущая карточка</Title>
                {/* <Card title={eng} extra={<a href="#">More</a>} style={{ width: 300 }}> */}
                <Card title={eng} style={{ width: 300 }}>
                <Text copyable>{rus}</Text>
                </Card>
            </div>
        );
    }
}

CurrentCard.contextType = FirebaseContext;

export default CurrentCard