import React, { Component } from 'react';

import { ClockCircleOutlined, HomeOutlined, SmileOutlined } from '@ant-design/icons';

import BackgroundBlock from '../../components/BackgroundBlock';
import ZButton from '../../components/ZButton';
import CardList from '../../components/CardList';
import ZFooter from '../../components/ZFooter';
import ZHeader from '../../components/ZHeader';
import Paragraph from '../../components/Paragraph';
import Section from '../../components/Section';

import { Layout, Button, Affix, Avatar } from 'antd';

import firstBackground from '../../assets/background.jpg';
import secondBackground from '../../assets/back2.jpg';

import FirebaseContext from '../../context/firebaseContext';

import s from './Home.module.scss';

const { Content, Header, Footer } = Layout;

class HomePage extends Component {
    state = {
        wordArr: [],
        user: null,
    }

    componentDidMount(){
        const { getUserCardsRef, auth } = this.context;

        this.setState({ 
            user: auth.currentUser,
        })

        getUserCardsRef().on('value', res => {
            this.setState({
                wordArr: res.val() || [],
            })
        })
    }

    handleDeletedItem = (id) => {
        const { wordArr } = this.state;

        const newWordArr = wordArr.filter(item => item.id !== id);

        // Так как всё равно мы записываем весь массив, то можно сделать просто set
        // но по хорошему надо удалять только один элемент через delete
        this.context.setUserCards(newWordArr);
        // .then(
        //     this.setState(({ wordArr }) => {
        //             return {
        //                 wordArr: newWordArr,
        //             }
        //         })       
        //     )
    }

    handleAddedItem = (eng, rus) => {
        const { wordArr } = this.state;
        const newWordArr = [...wordArr, { 
            id: +new Date(), 
            eng: eng,
            rus: rus,
        }];

        // TODO: Добавить проверку на ошибки
        this.context.setUserCards(newWordArr);
        // После замены once на on нам больше не нужно дополнительно вызывать сетстейт
        // .then(this.setState({ wordArr: newWordArr }))        
    }
    
    // TODO: Разобраться почему на появление слова влиял инпут из кардлиста!
    // setNewWord = () => {
    //     const { wordArr } = this.state;
    //     database.ref('/cards').set([...wordArr, { 
    //         id: +new Date(), 
    //         eng: 'mouse',
    //         rus: 'мышь',
    //     }]);
    // }

    handleSignOut = () => {
        const { signOut } = this.context;
        const { history } = this.props;
              
        signOut().then(history.push('/login'));
    }

    handleAddProfileInfo = () => {
        const { auth } = this.context;
        const user = auth.currentUser;
        console.log(this.context.user);

        // user.updateProfile({
        //         displayName: 'Alex Artemov',
        //         photoURL: 'https://secure.gravatar.com/avatar/2d40850ef7b180ba312f6b034dc9d394?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FA-2.png'})
        //     .then(function() {
        //         console.log('Update successful.');                
        //     })
        //     .catch(function(error) {
        //         console.log('An error happened.', error);
        //     });        
    }

    render() {
        const { wordArr, user } = this.state;
        
        return (
            <Layout>
                <Affix>
                    <Header>
                        <Button onClick={ this.handleSignOut }>Logout</Button>
                        &nbsp;
                        <Button onClick={ this.handleAddProfileInfo }>Add profile info</Button>                        
                        <Avatar size="large" src={user ? user.photoURL : null }/>  {user ? user.displayName : null }
                    </Header>
                </Affix>
                <Content>
                    <BackgroundBlock
                        backgroundImg={firstBackground}
                        fullHeight
                    >
                        <ZHeader white>
                            Время учить слова онлайн
                        </ZHeader>
                        <Paragraph white>
                            Используйте карточки для запоминания и пополняйте активный словарный запас.
                        </Paragraph>
                        <ZButton onClick={ () => this.refInput.current.focus() }>
                            Начать бесплатный урок
                        </ZButton>
                    </BackgroundBlock>
                    <Section className={s.textCenter}>
                        <ZHeader size="l">
                            Мы создали уроки, чтобы помочь вам увереннее разговаривать на английском языке
                        </ZHeader>
                        <div className={s.motivation}>
                            <div className={s.motivationBlock}>
                                <div className={s.icons}>
                                    <ClockCircleOutlined /> 
                                </div>
                                <Paragraph small>
                                    Учитесь, когда есть свободная минутка
                                </Paragraph>
                            </div>    
                            <div className={s.motivationBlock}>
                                <div className={s.icons}>
                                    <HomeOutlined />
                                </div>
                                <Paragraph small>
                                    Откуда угодно — дома, в&nbsp;офисе, в&nbsp;кафе
                                </Paragraph>
                            </div>
        
                            <div className={s.motivationBlock}>
                                <div className={s.icons}>
                                    <SmileOutlined />
                                </div>
                                <Paragraph small>
                                    Разговоры по-английски без&nbsp;неловких пауз и&nbsp;«mmm,&nbsp;how&nbsp;to&nbsp;say…»
                                </Paragraph>
                            </div>
                        </div>
                    </Section>
                    <Section bgColor="#f0f0f0" className={s.textCenter}>
                        <ZHeader size='l'>
                            Привет, { user ? user.displayName : 'Гость'}
                        </ZHeader>
                        <Paragraph>
                            Кликай по карточкам и узнавай новые слова, быстро и легко!
                        </Paragraph>
                        <CardList 
                            refEngInput={el => this.refInput = el}
                            onDeletedItem={ this.handleDeletedItem }
                            onAddedItem={ this.handleAddedItem } 
                            items={ wordArr }
                        />
                    </Section>
                    <BackgroundBlock
                        backgroundImg={secondBackground}
                    >
                        <ZHeader size="l" white>
                            Изучайте английский с персональным сайтом помощником
                        </ZHeader>
                        <Paragraph white>
                            Начните прямо сейчас
                        </Paragraph>
                    </BackgroundBlock>
                    <ZFooter/>
                </Content>
                <Footer>
                    Footer 
                </Footer>
            </Layout>
        );
    }
}

HomePage.contextType = FirebaseContext;

export default HomePage