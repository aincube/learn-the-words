import React, { Component } from 'react';
import { Layout, Form, Input, Button, Affix } from 'antd';
import FirebaseContext from '../../context/firebaseContext';

import s from './Login.module.scss';

const { Content, Header } = Layout;


class LoginPage extends Component {
    formRef = React.createRef();

    handleFormFinish = ({email, password}) => {
        const { signWithEmail, setUserUID } = this.context;
        const { history } = this.props;

        signWithEmail(email, password)
            .then(res => { 
                setUserUID(res.user.uid);
                localStorage.setItem('user', res.user.uid);
                console.log('res.user.uid was', res.user.uid);
                history.push('/')
            })
            .catch((error) =>{
                switch (error.code){
                    case 'auth/invalid-email':
                        this.formRef.current.setFields([
                            {
                              name: 'email',
                              errors: ['E-Mail wrong'],
                            },
                        ]);
                        break;
                    case 'auth/user-not-found':
                        this.formRef.current.setFields([
                            {
                              name: 'email',
                              errors: ['No user with this e-mail address'],
                            },
                        ]);
                        break;
                    case 'auth/wrong-password':
                        this.formRef.current.setFields([
                            {
                              name: 'password',
                              errors: ['Wrong password'],
                            },
                        ]);
                        break;
                    default:
                        console.log('Unknown error: ', error.code + ' : ' + error.message);
                }
            })
    };

    handleFormFailure = (errorMsg) => {
        console.log('Form error: ', errorMsg);
    };

    handleRegister = () => {
        const { auth, signWithEmail } = this.context;

        const email = this.formRef.current.getFieldValue('email');
        const password = this.formRef.current.getFieldValue('password');

        auth.createUserWithEmailAndPassword(email, password)
            .then((email, password) => {
                signWithEmail(email, password);
            })
            .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log('Register error: ', errorCode + ' ' + errorMessage);
        });    
    };

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };

        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };

        return (
            <Form
                ref={ this.formRef }
                {...layout}
                name="loginform"
                initialValues={{
                    remember: true,
                }}
                onFinish={ this.handleFormFinish }
                onFinishFailed={ this.handleFormFailure }
            >
                <Form.Item
                    label="E-mail"
                    name="email"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your email!',
                    },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Login
                    </Button>                    
                    <span>&nbsp; OR &nbsp;</span>
                    <Button type="primary" onClick={ this.handleRegister }>
                        Register
                    </Button>                    
                </Form.Item>
            </Form>
        );   
    };    

    render() {
        const { history } = this.props;
        if(localStorage.getItem('user')){
            history.push('/');
        }
        return (
            <Layout>
                {/* <Affix>
                    <Header >
                        <Button onClick={ this.handleSignOut }>
                            Выйти                            
                        </Button>
                    </Header>
                </Affix> */}
                <Content>
                    <div className={s.root}>
                        <div className={s.form_wrap}>
                            { this.renderForm() }
                        </div>
                    </div>
                </Content>
            </Layout>
        );     
    }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage