import React, { Component } from 'react';
import Card from '../Card';
import { Form, Input, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import getTranslateWord from '../../services/dictionary';

import s from './CardList.module.scss';

const { Search } = Input;

class CardList extends Component {
    state = {
        engvalue: '',
        rusvalue: '',
        svalue: '',
        label: '',
        isBusy: false,
        errormsg: '',
    }

    inputEngRef = React.createRef();

    constructor(props) {
        super(props);

        props.refEngInput && props.refEngInput(this.inputEngRef)
    }

    handleInputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value.toLowerCase()});
        console.log(this.state.svalue);
    }
    
    getTheWord = async () => {
        const { svalue } = this.state;

        const getWord = await getTranslateWord(svalue)

        this.setState(({ svalue }) => {
            return {
                label: `${svalue} - ${getWord.translate}`,
                isBusy: false,
                engvalue: svalue,
                rusvalue: getWord.translate,
                svalue: '',
            }
        })
    }

    handleSearch = async () => {
        const { svalue } = this.state;
        // Поле не пустое и слово состоит из 2 или более букв
        if ((svalue.length > 1) && (svalue.trim() !== '')) {
            this.setState({isBusy: true}, this.getTheWord);
        } else {
            // Переделать в алерт юзеру
            this.setState({ label: 'Введите слово из 2х букв и более'});
        }
    }

    handlePlusClick = () => {
        this.setState(( {engvalue , rusvalue} ) => {
            if(engvalue === ''){
                return {
                    label: 'Введите слово на английском!',
                }
            } else if (rusvalue === ''){
                return {
                    label: 'Введите слово на русском!',
                }
            }
            else {
                this.props.onAddedItem(engvalue, rusvalue);

                return {
                    label: `Добавлена карточка: ${engvalue} / ${rusvalue}`,
                    engvalue: '',
                    rusvalue: '',
                }    
            }
        })
    }

    render() {
        const { items, onDeletedItem } = this.props;
        const { svalue, label, isBusy } = this.state;
        
        return (
            <>
                <Form layout="inline">
                    <Search
                        name="svalue"
                        placeholder="Enter search text"
                        enterButton="Search"
                        size="medium"
                        value={ svalue }
                        loading={ isBusy }
                        onChange={this.handleInputChange}
                        onSearch={this.handleSearch}
                    />                        
                    <Divider>{ label }</Divider>
                    <Input.Group compact>
                        <Input 
                            name="engvalue"
                            ref={this.inputEngRef}
                            style={{ width: '33%' }} 
                            placeholder="English word" 
                            value={this.state.engvalue}
                            onChange={this.handleInputChange}
                        />
                        <Input 
                            name="rusvalue"
                            style={{ width: '33%' }} 
                            placeholder="Русское слово"
                            value={this.state.rusvalue}
                            onChange={this.handleInputChange}
                            addonAfter={<PlusOutlined onClick={this.handlePlusClick} />} 
                        />
                    </Input.Group>
                </Form>
                <Divider/>
                {/* <form className={s.form} onSubmit={this.handleSubmitForm}>
                    <input 
                        name="engvalue"
                        type="text" 
                        placeholder="English word"
                        value={this.state.engvalue}
                        onChange={this.handleInputChange}
                    />
                    &nbsp;
                    <input 
                        name="rusvalue"
                        type="text" 
                        placeholder="Русское слово"
                        value={this.state.rusvalue}
                        onChange={this.handleInputChange}
                    />
                    &nbsp;
                    <button>Add new word</button>
                </form> */}
                <div className={s.root}>
                    {
                        items.map(({ eng, rus, id }, index) => (
                            <Card
                                onDeleted={() => { onDeletedItem(id) }}
                                key={id}
                                eng={eng}
                                rus={rus}
                                index={index}
                            />
                        ))
                    }
                </div>
            </>
        );
    }
}

export default CardList;
