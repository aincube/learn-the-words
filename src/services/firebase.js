import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FB_apiKey,
    authDomain: process.env.REACT_APP_FB_authDomain,
    databaseURL: process.env.REACT_APP_FB_databaseURL,
    projectId: process.env.REACT_APP_FB_projectId,
    storageBucket: process.env.REACT_APP_FB_storageBucket,
    messagingSenderId: process.env.REACT_APP_FB_messagingSenderId,
    appId: process.env.REACT_APP_FB_appId
  };

class Firebase {
    constructor() {
        firebase.initializeApp(firebaseConfig);

        this.auth = firebase.auth();
        this.database = firebase.database();
        this.userUID = null;
    }

    setUserUID = (uid) => this.userUID = uid;

    signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
    signOut = () => this.auth.signOut();

    getUserCardsRef = () => this.database.ref(`/cards/${this.userUID}`);
    getUserCurrentCardRef = (id) => this.database.ref(`/cards/${this.userUID}/${id}`);
    
    setUserCards = (items) => this.getUserCardsRef().set(items);
}

export default Firebase;